var _WM_APP_PROPERTIES = {
  "activeTheme" : "default",
  "defaultLanguage" : "en",
  "displayName" : "Autocomplete",
  "homePage" : "Main",
  "name" : "Autocomplete",
  "platformType" : "DEFAULT",
  "supportedLanguages" : "en",
  "type" : "PREFAB",
  "version" : "1.0"
};